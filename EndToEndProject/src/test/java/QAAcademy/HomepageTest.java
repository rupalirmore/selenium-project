package QAAcademy;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import PageObject.LandingPage;
import Resources.Base;

public class HomepageTest extends Base {
	
	public static Logger log = LogManager.getLogger(Base.class.getName());
	
	
	@Test
	public void Homepagenavigation() throws IOException{
		driver= Setup();
		driver.get(sbaseURL);
		LandingPage lp = new LandingPage(driver);
		lp.courseNav().click();
		log.info(driver.getTitle());
		log.info(driver.getCurrentUrl());
		/*System.out.println(driver.getTitle());
		System.out.println(driver.getCurrentUrl());
		*/
		driver=Teardown();
	}
	
	
}
