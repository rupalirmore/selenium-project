package QAAcademy;

import java.io.IOException;

import junit.framework.Assert;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import PageObject.LandingPage;
import PageObject.LoginPage;
import Resources.Base;

public class LoginpageTest extends Base {
	
	public static Logger log = LogManager.getLogger(Base.class.getName());
	
	@Test(dataProvider="getData")
	public void validateLogin(String username, String password) throws IOException{
		driver= Setup();
		driver.get(sbaseURL);
		log.info("Successfully Navigated to Homepage");
	
		LandingPage lp = new LandingPage(driver);
		lp.getlogin().click();
		System.out.println(driver.getTitle());
		System.out.println(driver.getCurrentUrl());
		LoginPage lop =  new LoginPage(driver);
		Assert.assertEquals(lop.TextCenter().getText().trim(), "Log In to QaClickAcademy");
		lop.getemail().sendKeys(username);
		lop.getpassword().sendKeys(password);
		lop.getlogin().click();
		System.out.println(driver.getTitle());
		System.out.println(driver.getCurrentUrl());
		driver=Teardown();	
	}
	
	
	
	
	@DataProvider
	public Object[][] getData(){
	
	Object[][]data = new Object[1][2];
	data [0][0]="abc@gmail.com";
	data [0][1]="Falooda";
	
	return data;
	
	}
	
	
	
	
	
}
