package QAAcademy;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import PageObject.LandingPage;
import Resources.Base;

public class ValidateNavigationBar extends Base {

	public static Logger log = LogManager.getLogger(Base.class.getName());
	@BeforeTest
	public void initializeBrowser() throws IOException{
		driver= Setup();
		driver.get(sbaseURL);
		log.info("Successfully Navigated to Homepage");
	}
	
	
	@Test
	public void ValidateNavBar() throws IOException{
		
		LandingPage lp = new LandingPage(driver);
		//Assert.assertTrue(lp.getNavigationBar().isDisplayed());
		Assert.assertFalse(lp.getNavigationBar().isDisplayed());
		log.info("Successfully validated NavigationBar");
		driver=Teardown();
	}
	
	
	
}
