package PageObject;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage {
	
	
	public WebDriver driver;
	
	By email = By.id("user_email");
	By password = By.xpath("//input[@id='user_password']");
	By login = By.cssSelector("input[value*='Log In']");
	By forPass = By.cssSelector("a[href*='secure user new']");
	By createAcc = By.cssSelector("a[href*='secure users sign_up']");
	By iFrame = By.cssSelector(".content-box");
	By alertInvalid = By.cssSelector(".alert.alert-danger");
	By textCenter = By.tagName("h1");
      
     
      
	
	

	public LoginPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
	}

	public WebElement getemail(){
		return driver.findElement(email);
		}
	
	public WebElement getpassword(){
		return driver.findElement(password);
		}
	
	public WebElement getlogin(){
		return driver.findElement(login);
	}
	
	public WebElement ForgotPassword(){
		return driver.findElement(forPass);
	}
	
	public WebElement createAccount(){
		return driver.findElement(createAcc);
	}
	
	public WebElement Iframe(){
		return driver.findElement(iFrame);
	}
	
	public WebElement TextCenter(){
		return driver.findElement(textCenter);
	}
	
	public WebElement AlertInvalid(){
		return driver.findElement(alertInvalid);
	}
}



