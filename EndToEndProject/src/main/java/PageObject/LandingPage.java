package PageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LandingPage {
	
	public WebDriver driver;
	
	By signin= By.xpath(".//*[@id='homepage']/header/div[1]/div/nav/ul/li[4]/a");
	By navBar = By.xpath(".//*[@id='homepage']/header/div[2]/div/nav");
	By course = By.xpath(".//*[@id='homepage']/header/div[2]/div/nav/ul/li[2]/a");
	
	public LandingPage() {
		// TODO Auto-generated constructor stub
		this.driver = driver;
	}

	public LandingPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
	}

	public WebElement getNavigationBar(){
		return driver.findElement(navBar);
		}
	
	public WebElement getlogin(){
	return driver.findElement(signin);
	}
	
	public WebElement courseNav(){
		return driver.findElement(course);
		}
}
